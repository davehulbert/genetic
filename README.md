# Genetic

Finds good solutions to a problem, using operations inspired by natural selection.

## Terms

* Solution: a candidate item that can solve a problem
* Fitness: how well a candidate solves a problem
* Genes: components of a solution
* Population or Generation: a pool of solutions
* Mutate: changing the genes of a solution
* Combination: combining 2 solutions to make a new one

https://en.wikipedia.org/wiki/Genetic_algorithm for more.

## Usage

    $population = [
        [0, 0, 1],
        [2, 0, 0],
    ];
    $farm = FarmFactory::create();
    
    $nextGeneration = $farm->harvest($population);
    $nextGeneration = $farm->harvest($population);
    $nextGeneration = $farm->harvest($population);
    $best = $nextGeneration[0];

## Implementation

* Solutions are PHP arrays
* Solution's genes must be floats
* A solution's fitness is a float
* Solutions are ordered by fitness
