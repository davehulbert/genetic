<?php

namespace Dave\Genetic\Combination;

interface CombinationInterface
{
    /**
     * Genetic operator that produces multiple children from 2 parents
     * could take more than 2 parents
     */
    public function __invoke(array $a, array $b): array;
}
