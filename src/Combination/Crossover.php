<?php

namespace Dave\Genetic\Combination;

final class Crossover implements CombinationInterface
{
    public function __invoke(array $a, array $b): array
    {
        $child = [];

        foreach ($a as $k => $v) {
            $child[$k] = random_int(0, 1) ? $a[$k] : $b[$k];
        }

        return [$child];
    }
}
