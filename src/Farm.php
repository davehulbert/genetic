<?php

namespace Dave\Genetic;

use Dave\Genetic\SolutionMutators\SolutionMutatorInterface;
use Dave\Genetic\SolutionMutators\NoOpSolutionMutator;
use Generator;

final class Farm
{
    private $fitness;
    private $mutator;
    private $maxPopulation;
    private $offspringPerParents;

    /**
     * @param callable                      $fitness evaluates an item and returns something that can be compared
     * @param SolutionMutatorInterface|null $mutator mutates a parent
     * @param int                           $maxPopulation
     * @param int                           $offspringPerParents
     */
    public function __construct(
        callable $fitness,
        SolutionMutatorInterface $mutator = null,
        int $maxPopulation = 4,
        $offspringPerParents = 2
    )
    {
        $this->fitness = $fitness;
        $this->maxPopulation = $maxPopulation;
        $this->offspringPerParents = $offspringPerParents;
        $this->mutator = $mutator ?? new NoOpSolutionMutator();
    }

    public function harvest(array $population): Generator
    {
        yield $population = $this->cull($population);

        $farmer = new Farmer($this->offspringPerParents, $this->mutator);

        while (true) {
            $population = $farmer->breed($population);

            yield $population = $this->cull($population);
        }
    }

    private function cull(array $population): array
    {
        usort($population, function($a, $b)  {
            $fitness = $this->fitness;
            return $fitness($b) <=> $fitness($a); // fittest first
        });

        return array_slice($population, 0, $this->maxPopulation);
    }
}
