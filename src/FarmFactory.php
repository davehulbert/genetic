<?php

namespace Dave\Genetic;

use Dave\Genetic\Fitness\SumFitness;
use Dave\Genetic\SolutionMutators\DoNotMutateSoMuchMutator;
use Dave\Genetic\SolutionMutators\MaybeInvertGenes;

final class FarmFactory
{
    public static function create(): Farm
    {
        return new Farm(new SumFitness(), new DoNotMutateSoMuchMutator(new MaybeInvertGenes()));
    }
}
