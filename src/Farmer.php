<?php

namespace Dave\Genetic;

use Dave\Genetic\Combination\Crossover;

final class Farmer
{
    private $offspringPerParents;

    private $mutator;

    public function __construct(int $offspringPerParents, callable $mutator)
    {
        $this->offspringPerParents = $offspringPerParents;
        $this->mutator = $mutator;
    }

    /**
     * This farmer breeds by mutating 2 parents, then crossing over their genes randomly to create children
     *
     * Mutators are always ran
     *
     * @param array $oldGeneration
     *
     * @return array new population
     */
    public function breed(array $oldGeneration): array
    {
        $mutator = $this->mutator;
        $population = $oldGeneration; // old generation stay alive
        shuffle($population);

        // breed A with B, B with C, etc.
        foreach ($oldGeneration as $k => $result) {

            if (!isset($oldGeneration[$k+1])) {continue;}

            $a = $oldGeneration[$k];
            $b = $oldGeneration[$k+1];

            for ($i = 0; $i < $this->offspringPerParents; $i++) {
                $population[] = self::crossover($a, $b);
                $population[] = self::crossover($mutator($a), $b);
                $population[] = self::crossover($a, $mutator($b));
                $population[] = self::crossover($mutator($a), $mutator($b));
            }
        }

        // to do: unique children
        return $population;
    }

    private static function crossover(array $a, array $b): array
    {
        return (new Crossover())->__invoke($a, $b)[0];
    }
}
