<?php


namespace Dave\Genetic\Fitness;

/**
 * Evaluate a solution and give it a score
 */
interface FitnessInterface
{
    public function __invoke(array $solution): float;
}
