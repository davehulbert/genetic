<?php

namespace Dave\Genetic\Fitness;

/**
 * Evaluate fitness based on the sum of all genes
 */
final class SumFitness implements FitnessInterface
{

    public function __invoke(array $solution): float
    {
        return array_sum($solution);
    }
}
