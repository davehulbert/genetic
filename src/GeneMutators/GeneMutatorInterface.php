<?php

namespace Dave\Genetic\GeneMutators;

interface GeneMutatorInterface
{
    public function __invoke(float $gene): float;
}
