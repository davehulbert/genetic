<?php

namespace Dave\Genetic\GeneMutators;

final class InvertGeneMutator implements GeneMutatorInterface
{
    public function __invoke(float $gene): float
    {
        return -$gene;
    }
}
