<?php

namespace Dave\Genetic\SolutionMutators;

/**
 * Reduces the chance of a solution being mutated by 50%
 */
final class DoNotMutateSoMuchMutator implements SolutionMutatorInterface
{
    private $solutionMutator;

    public function __construct(SolutionMutatorInterface $solutionMutator)
    {
        $this->solutionMutator = $solutionMutator;
    }

    public function __invoke(array $solution): array
    {
        $solutionMutator = $this->solutionMutator;
        return random_int(0, 1) ? $solutionMutator($solution) : $solution;
    }
}
