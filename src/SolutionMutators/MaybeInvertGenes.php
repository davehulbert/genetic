<?php

namespace Dave\Genetic\SolutionMutators;

use Dave\Genetic\GeneMutators\InvertGeneMutator;

/**
 * Takes a gene and maybe switches it between positive and negative
 */
final class MaybeInvertGenes implements SolutionMutatorInterface
{
    public function __invoke(array $solution): array
    {
        return (new MaybeMutateGenesMutator(new InvertGeneMutator()))($solution);
    }
}
