<?php

namespace Dave\Genetic\SolutionMutators;

use Dave\Genetic\GeneMutators\GeneMutatorInterface;

/**
 * Applies a mutator to individual genes 50% of the time
 */
final class MaybeMutateGenesMutator implements SolutionMutatorInterface
{
    private $geneMutator;

    public function __construct(GeneMutatorInterface $geneMutator)
    {
        $this->geneMutator = $geneMutator;
    }

    public function __invoke(array $solution): array
    {
        $geneMutator = $this->geneMutator;
        $mutated = [];

        foreach ($solution as $k => $gene) {
            $mutated[$k] = random_int(0, 1) ? $geneMutator($gene) : $gene;
        }

        return $mutated;
    }
}
