<?php

namespace Dave\Genetic\SolutionMutators;

final class NoOpSolutionMutator implements SolutionMutatorInterface
{

    public function __invoke(array $solution): array
    {
        return $solution;
    }
}
