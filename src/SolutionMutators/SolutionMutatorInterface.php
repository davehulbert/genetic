<?php

namespace Dave\Genetic\SolutionMutators;

/**
 * Mutates a whole solution
 */
interface SolutionMutatorInterface
{
    public function __invoke(array $solution): array;
}
