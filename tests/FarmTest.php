<?php

namespace Dave\Genetic;

use Dave\Genetic\Fitness\SumFitness;
use Dave\Genetic\SolutionMutators\DoNotMutateSoMuchMutator;
use Dave\Genetic\SolutionMutators\MaybeInvertGenes;
use PHPUnit\Framework\TestCase;

final class FarmTest extends TestCase
{
    public function testHarvest()
    {
        // evaluating a solution and giving it a score
        // todo: add an adaptor to use PHPUnit itself for measuring fitness?
        $fitness = static function(array $solution): float {
            // difficult fitness evaluation, that's hard to guess
            // for best results, 0 should be high, 1, should be low and 2 should be even
            // return $solution[0] - ($solution[1] * $solution[2] % 2);
            return $solution[0] + $solution[1] + $solution[2];
        };

        // lots of solutions, randomly generated, none very good, covering whole search space
        $population = [
            [0, 0, 0],
            [1, 0, 0],
            [0, 2, -2],
            [-1, 0, 3],
            [4, 0, -2],
            [-3, 2, -3],
            [0, 3, 0],
        ];

        $farm = FarmFactory::create();

        foreach ($farm->harvest($population) as $k => $population) {
            echo "Best solution in generation $k has a fitness of " .$fitness($population[0]).PHP_EOL;

            if ($k > 20) {
                break;
            }
        }
        print_r($population[0]);
        self::assertGreaterThan(3, $fitness($population[0]));
    }

    public function testSuperString()
    {
        $strings = [
            'ABC',
             'BCD',
             'BC',
        ];

        $population = [
            [],
        ];

    }
}
